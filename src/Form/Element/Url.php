<?php

namespace Drupal\oop_forms\Form\Element;


class Url extends TextElement {

  /**
   * Url constructor.
   */
  public function __construct() {
    return parent::__construct('url');
  }
}
