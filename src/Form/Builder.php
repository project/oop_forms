<?php

namespace Drupal\oop_forms\Form;

use Drupal\oop_forms\Form\Element\EntityAutocomplete;
use Drupal\oop_forms\Form\Element\Item;
use Drupal\oop_forms\Form\Element\Link;
use Drupal\oop_forms\Form\Element\MachineName;
use Drupal\oop_forms\Form\Element\Number;
use Drupal\oop_forms\Form\Element\Select;
use Drupal\oop_forms\Form\Element\TextField;
use Drupal\oop_forms\Form\Element\Url;

/**
 * Form Builder class.
 */
class Builder {

  /**
   * Create textfield.
   *
   * @return \Drupal\oop_forms\Form\Element\TextField
   *   Returns the textfield.
   */
  public function createTextField(): TextField {
    return new TextField();
  }

  /**
   * Create Url.
   *
   * @return \Drupal\oop_forms\Form\Element\Url
   *   Returns the Url.
   */
  public function createUrl(): Url {
    return new Url();
  }

  /**
   * Create link.
   *
   * @return \Drupal\oop_forms\Form\Element\Link
   *   Returns the link.
   */
  public function createLink(): Link {
    return new Link();
  }

  /**
   * Create item.
   *
   * @return \Drupal\oop_forms\Form\Element\Item
   *   Returns the item.
   */
  public function createItem(): Item {
    return new Item();
  }

  /**
   * Create autocomplete.
   *
   * @return \Drupal\oop_forms\Form\Element\EntityAutocomplete
   *   Returns the autocomplete.
   */
  public function createEntityAutocomplete(): EntityAutocomplete {
    return new EntityAutocomplete();
  }

  /**
   * Create the number.
   *
   * @return \Drupal\oop_forms\Form\Element\Number
   *   Returns the number.
   */
  public function createNumber() {
    return new Number();
  }

  /**
   * Create the machine name.
   *
   * @return \Drupal\oop_forms\Form\Element\MachineName
   *   Returns the machine name.
   */
  public function createMachineName() {
    return new MachineName();
  }

  /**
   * Create the select.
   *
   * @return \Drupal\oop_forms\Form\Element\Select
   *  Returns the select.
   */
  public function createSelect() {
    return new Select();
  }

}
